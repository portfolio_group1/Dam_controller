% !!! Clear memory space & terminal !!!
clear;
clc;

% !!! Linguistic variables !!!
TEM_lng_var = [-20:0.01:40];

% !!! TEM linguistic variable's terms !!!
u_TEM_BZ_term = [ones(1, 901) zeros(1, 5100)];
TEM_BZ_term_apexes = [-20 -11];
u_TEM_BZ_term_apexes = ones(1, 2);

u_TEM_Z_term = [zeros(1, 901) ones(1, 999) zeros(1, 4101)];
TEM_Z_term_apexes = [-1];
u_TEM_Z_term_apexes = ones(1, 1);

u_TEM_ML_term = [zeros(1, 1900) ones(1, 999) zeros(1, 3102)];
TEM_ML_term_apexes = [9];
u_TEM_ML_term_apexes = ones(1, 1);

u_TEM_L_term = [zeros(1, 2899) ones(1, 999) zeros(1, 2103)];
TEM_L_term_apexes = [19];
u_TEM_L_term_apexes = ones(1, 1);

u_TEM_C_term = [zeros(1, 3898) ones(1, 999) zeros(1, 1104)];
TEM_C_term_apexes = [29];
u_TEM_C_term_apexes = ones(1, 1);

u_TEM_BC_term = [zeros(1, 4897) ones(1, 999) zeros(1, 105)];
TEM_BC_term_apexes = [39];
u_TEM_BC_term_apexes = ones(1, 1);

u_TEM_G_term = [zeros(1, 5896) ones(1, 105)];
TEM_G_term_apexes = [40];
u_TEM_G_term_apexes = ones(1, 1);

% !!! The TEM linguistic variable's region !!!
figure('name',"Input terms for the TEM decoder model");

% !!! TEM linguistic variable apexes' labels !!!
TEM_BZ_term_apexes_plot_labels = {
    strcat('(',string(TEM_BZ_term_apexes(1)),';',string(round(u_TEM_BZ_term_apexes(1),2)),')')
    strcat('(',string(TEM_BZ_term_apexes(2)),';',string(round(u_TEM_BZ_term_apexes(2),2)),')')
    };

TEM_Z_term_apexes_plot_labels = {
    strcat('(',string(TEM_Z_term_apexes(1)),';',string(round(u_TEM_Z_term_apexes(1),2)),')')    
    };

TEM_ML_term_apexes_plot_labels = {
    strcat('(',string(TEM_ML_term_apexes(1)),';',string(round(u_TEM_ML_term_apexes(1),2)),')')    
    };

TEM_L_term_apexes_plot_labels = {
    strcat('(',string(TEM_L_term_apexes(1)),';',string(round(u_TEM_L_term_apexes(1),2)),')')    
    };

TEM_C_term_apexes_plot_labels = {
    strcat('(',string(TEM_C_term_apexes(1)),';',string(round(u_TEM_C_term_apexes(1),2)),')')    
    };

TEM_BC_term_apexes_plot_labels = {
    strcat('(',string(TEM_BC_term_apexes(1)),';',string(round(u_TEM_BC_term_apexes(1),2)),')')    
    };

TEM_G_term_apexes_plot_labels = {
    strcat('(',string(TEM_G_term_apexes(1)),';',string(round(u_TEM_G_term_apexes(1),2)),')')    
    };

% !!! Parameters of the TEM linguistic variable's figure !!!
title('TEM terms', 'Position', [9.5 1.04 0]);
xlabel('Temperature');
ylabel('μ_{Temperature}');
xticks([-20:5:40]);
hold on;

% !!! Drawing the TEM linguistic variable's terms !!!
TEM_BZ_term_plot = plot(TEM_lng_var, u_TEM_BZ_term, 'LineStyle', '-', 'Color', '#0432FF');
TEM_BZ_term_apexes_plot = plot(TEM_BZ_term_apexes, u_TEM_BZ_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#0432FF');
text(TEM_BZ_term_apexes, u_TEM_BZ_term_apexes, TEM_BZ_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

TEM_Z_term_plot = plot(TEM_lng_var, u_TEM_Z_term, 'LineStyle', '-', 'Color', '#008F00');
TEM_Z_term_apexes_plot = plot(TEM_Z_term_apexes, u_TEM_Z_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#008F00');
text(TEM_Z_term_apexes, u_TEM_Z_term_apexes, TEM_Z_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

TEM_ML_term_plot = plot(TEM_lng_var, u_TEM_ML_term, 'LineStyle', '-', 'Color', '#0096FF');
TEM_ML_term_apexes_plot = plot(TEM_ML_term_apexes, u_TEM_ML_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#0096FF');
text(TEM_ML_term_apexes, u_TEM_ML_term_apexes, TEM_ML_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

TEM_L_term_plot = plot(TEM_lng_var, u_TEM_L_term, 'LineStyle', '-', 'Color', '#9437FF');
TEM_L_term_apexes_plot = plot(TEM_L_term_apexes, u_TEM_L_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#9437FF');
text(TEM_L_term_apexes, u_TEM_L_term_apexes, TEM_L_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

TEM_C_term_plot = plot(TEM_lng_var, u_TEM_C_term, 'LineStyle', '-', 'Color', '#FF9300');
TEM_C_term_apexes_plot = plot(TEM_C_term_apexes, u_TEM_C_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#FF9300');
text(TEM_C_term_apexes, u_TEM_C_term_apexes, TEM_C_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

TEM_BC_term_plot = plot(TEM_lng_var, u_TEM_BC_term, 'LineStyle', '-', 'Color', '#FF7E79');
TEM_BC_term_apexes_plot = plot(TEM_BC_term_apexes, u_TEM_BC_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#FF7E79');
text(TEM_BC_term_apexes, u_TEM_BC_term_apexes, TEM_BC_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

TEM_G_term_plot = plot(TEM_lng_var, u_TEM_G_term, 'LineStyle', '-', 'Color', '#A9A9A9');
TEM_G_term_apexes_plot = plot(TEM_G_term_apexes, u_TEM_G_term_apexes, 'LineStyle', 'none','Marker','s','Color', '#A9A9A9');
text(TEM_G_term_apexes, u_TEM_G_term_apexes, TEM_G_term_apexes_plot_labels, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');

% !!! Drawing the TEM linguistic variable figure's legend !!!
TEM_lng_var_plots_legend = [
    TEM_BZ_term_plot
    TEM_Z_term_plot
    TEM_ML_term_plot
    TEM_L_term_plot
    TEM_C_term_plot
    TEM_BC_term_plot
    TEM_G_term_plot
    ];

legend(TEM_lng_var_plots_legend, 'BZ', 'Z', 'ML', 'L', 'C', 'BC', 'G','Location', 'eastoutside');
hold off;