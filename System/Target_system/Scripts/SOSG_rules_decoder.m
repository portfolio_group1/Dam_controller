function decoded_SOSG_rule_vector_out = SOSG_rules_decoder(SOSG_rule_vector_size,SOSG_rule_vector_in,in_terms_ling_val_belong)
    SOSG_rule_vector_out_temp = single(zeros(1,SOSG_rule_vector_size));
    % PW terms' indexes
    PW_idx = 1;
    PW_BN_idx = 1;
    PW_N_idx = 2;
    PW_S_idx = 3;
    PW_W_idx = 4;
    PW_ALM_idx = 5;    
    % TEM terms' indexes
    TEM_idx = 2;
    TEM_N_idx = 1;
    TEM_S_idx = 2;
    TEM_W_idx = 3;
    % DPE terms' indexes
    DPE_idx = 3;
    DPE_N_idx = 1;
    DPE_U_idx = 2;
    DPE_W_idx = 3;
    
    
    % Decode SOSG's rule vector
    for i=1:1:SOSG_rule_vector_size
        if (SOSG_rule_vector_in(i) == 1)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_BN_idx);
        elseif (SOSG_rule_vector_in(i) == 2)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_N_idx);
        elseif (SOSG_rule_vector_in(i) == 3)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_S_idx);
        elseif (SOSG_rule_vector_in(i) == 4)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_W_idx);
        elseif (SOSG_rule_vector_in(i) == 5)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_ALM_idx);
        elseif (SOSG_rule_vector_in(i) == 6)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_N_idx);
        elseif (SOSG_rule_vector_in(i) == 7)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_S_idx);
        elseif (SOSG_rule_vector_in(i) == 8)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_W_idx);
        elseif (SOSG_rule_vector_in(i) == 9)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(DPE_idx,DPE_N_idx);
        elseif (SOSG_rule_vector_in(i) == 10)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(DPE_idx,DPE_U_idx);
        elseif (SOSG_rule_vector_in(i) == 11)
            SOSG_rule_vector_out_temp(i) = in_terms_ling_val_belong(DPE_idx,DPE_W_idx);
        end
    end
    % Return decoded SOSG's rule vector
    decoded_SOSG_rule_vector_out = SOSG_rule_vector_out_temp;
end