function SOSA = Block_processor(SOSA_aggr_out_terms)
    defuzz_numerator = single(0);
    defuzz_denominator = single(0);
    
    
    % Calculate defuzzification's numerator & denominator
    for i=1:1:size(SOSA_aggr_out_terms,2)
        defuzz_numerator = (defuzz_numerator+((i-1)*SOSA_aggr_out_terms(i)));
        defuzz_denominator = (defuzz_denominator+SOSA_aggr_out_terms(i));
    end    
    % Return SOSA's sharp value
    SOSA = single((defuzz_numerator/defuzz_denominator)/100);
end