function SUP_resolv_concl_vals = Block_processor(SUP_concl_vals)
    % SUP conclusions values sizes
    SUP_Z_concl_size = 21;
    SUP_N_concl_size = 7;
    SUP_S_concl_size = 6;
    SUP_W_concl_size = 7;
    SUP_NS_concl_size = 4;
    % SUP terms' indexes
    SUP_Z_concl_idx = 1;
    SUP_N_concl_idx = 2;
    SUP_S_concl_idx = 3;
    SUP_W_concl_idx = 4;
    SUP_NS_concl_idx = 5;
    % SUP terms resolved conclusions values
    SUP_Z_resolv_val = single(0);
    SUP_N_resolv_val = single(0);
    SUP_S_resolv_val = single(0);
    SUP_W_resolv_val = single(0);
    SUP_NS_resolv_val = single(0);
    
    % Resolving SUP Z term conclusions' conflict
    if (SUP_Z_concl_size > 1)
        SUP_Z_resolv_val = max(SUP_concl_vals(SUP_Z_concl_idx,:));
    elseif (SUP_Z_concl_size == 1)
        SUP_Z_resolv_val = SUP_concl_vals(SUP_Z_concl_idx,1);
    else
        SUP_Z_resolv_val = 0;
    end
    % Resolving SUP N term conclusions' conflict
    if (SUP_N_concl_size > 1)        
        SUP_N_resolv_val = max(SUP_concl_vals(SUP_N_concl_idx,:));
    elseif (SUP_N_concl_size == 1)
        SUP_N_resolv_val = SUP_concl_vals(SUP_N_concl_idx,1);
    else
        SUP_N_resolv_val = 0;
    end
    % Resolving SUP S term conclusions' conflict
    if (SUP_S_concl_size > 1)        
        SUP_S_resolv_val = max(SUP_concl_vals(SUP_S_concl_idx,:));
    elseif (SUP_S_concl_size == 1)
        SUP_S_resolv_val = SUP_concl_vals(SUP_S_concl_idx,1);
    else
        SUP_S_resolv_val = 0;
    end
    % Resolving SUP W term conclusions' conflict
    if (SUP_W_concl_size > 1)
        SUP_W_resolv_val = max(SUP_concl_vals(SUP_W_concl_idx,:));
    elseif (SUP_W_concl_size == 1)
        SUP_W_resolv_val = SUP_concl_vals(SUP_W_concl_idx,1);
    else
        SUP_W_resolv_val = 0;
    end
    % Resolving SUP NS term conclusions' conflict
    if (SUP_NS_concl_size > 1)
        SUP_NS_resolv_val = max(SUP_concl_vals(SUP_NS_concl_idx,:));
    elseif (SUP_NS_concl_size == 1)
        SUP_NS_resolv_val = SUP_concl_vals(SUP_NS_concl_idx,1);
    else
        SUP_NS_resolv_val = 0;
    end
    % Return SUP resolved conclusions values
    SUP_resolv_concl_vals = [
        SUP_Z_resolv_val
        SUP_N_resolv_val
        SUP_S_resolv_val
        SUP_W_resolv_val
        SUP_NS_resolv_val
        ];
end