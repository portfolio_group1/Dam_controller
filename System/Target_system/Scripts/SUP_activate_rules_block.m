function SUP_concl_vals = Block_processor(SUP_rules,in_terms_ling_val_belong)
    % SUP conclusions values sizes
    SUP_Z_concl_size = 21;
    SUP_N_concl_size = 7;
    SUP_S_concl_size = 6;
    SUP_W_concl_size = 7;
    SUP_NS_concl_size = 4;
    % SUP conclusions values
    SUP_Z_concl_vals = single(zeros(1,21));
    SUP_N_concl_vals = single(zeros(1,21));
    SUP_S_concl_vals = single(zeros(1,21));
    SUP_W_concl_vals = single(zeros(1,21));
    SUP_NS_concl_vals = single(zeros(1,21));
          
    % Calculate Z term's conclusions values
    SUP_rule_vector_size = 3;
    SUP_Z_rule_vector = zeros(1,SUP_rule_vector_size);
    for i=1:1:SUP_Z_concl_size
        SUP_Z_rule_vector = SUP_rules(i,:);
        % Decode SUP's rule vector
        SUP_Z_rule_vector = SUP_rules_decoder(SUP_rule_vector_size,SUP_Z_rule_vector,in_terms_ling_val_belong);
        % Calculate SUP Z term's conclusion value
        SUP_Z_concl_vals(i) = prod(SUP_Z_rule_vector);
    end    
    % Calculate N term's conclusions values
    SUP_rule_vector_size = 3;
    SUP_N_rule_vector = zeros(1,SUP_rule_vector_size);
    for i=1:1:SUP_N_concl_size
        SUP_N_rule_vector = SUP_rules((i+SUP_Z_concl_size),:);
        % Decode SUP's rule vector
        SUP_N_rule_vector = SUP_rules_decoder(SUP_rule_vector_size,SUP_N_rule_vector,in_terms_ling_val_belong);
        % Calculate SUP N term's conclusion value
        SUP_N_concl_vals(i) = prod(SUP_N_rule_vector);
    end
    % Calculate S term's conclusions values
    SUP_rule_vector_size = 3;
    SUP_S_rule_vector = zeros(1,SUP_rule_vector_size);
    for i=1:1:SUP_S_concl_size
        SUP_S_rule_vector = SUP_rules((i+SUP_Z_concl_size+SUP_N_concl_size),:);
        % Decode SUP's rule vector
        SUP_S_rule_vector = SUP_rules_decoder(SUP_rule_vector_size,SUP_S_rule_vector,in_terms_ling_val_belong);
        % Calculate SUP S term's conclusion value
        SUP_S_concl_vals(i) = prod(SUP_S_rule_vector);
    end
    % Calculate W term's conclusions values
    SUP_rule_vector_size = 3;
    SUP_W_rule_vector = zeros(1,SUP_rule_vector_size);
    for i=1:1:SUP_W_concl_size
        SUP_W_rule_vector = SUP_rules((i+SUP_Z_concl_size+SUP_N_concl_size+SUP_S_concl_size),:);
        % Decode SUP's rule vector
        SUP_W_rule_vector = SUP_rules_decoder(SUP_rule_vector_size,SUP_W_rule_vector,in_terms_ling_val_belong);
        % Calculate SUP S term's conclusion value
        SUP_W_concl_vals(i) = prod(SUP_W_rule_vector);
    end
    % Calculate NS term's conclusions values
    SUP_rule_vector_size = 3;
    SUP_NS_rule_vector = zeros(1,SUP_rule_vector_size);
    for i=1:1:SUP_NS_concl_size
        SUP_NS_rule_vector = SUP_rules((i+SUP_Z_concl_size+SUP_N_concl_size+SUP_S_concl_size+SUP_W_concl_size),:);
        % Decode SUP's rule vector
        SUP_NS_rule_vector = SUP_rules_decoder(SUP_rule_vector_size,SUP_NS_rule_vector,in_terms_ling_val_belong);
        % Calculate SUP S term's conclusion value
        SUP_NS_concl_vals(i) = prod(SUP_NS_rule_vector);
    end
    % Return SUP conclusions values
    SUP_concl_vals = [
        SUP_Z_concl_vals
        SUP_N_concl_vals
        SUP_S_concl_vals
        SUP_W_concl_vals
        SUP_NS_concl_vals
        ];
end