function SOSA_rules = Block_processor()
    % SOSA Z term's rules 33
    SOSA_Z_rules = single([
        [1 6 11]
        [1 6 10]
        [1 6 9]
        [1 7 11]
        [1 7 10]
        [1 7 9]
        [1 8 11]
        [1 8 10]
        [1 8 9]
        [2 6 11]
        [2 6 10]
        [2 6 9]
        [2 7 11]
        [2 7 10]
        [2 7 9]
        [2 8 11]
        [2 8 10]
        [2 8 9]
        [3 6 11]
        [3 6 10]
        [3 6 9]
        [3 7 11]
        [3 7 10]
        [3 7 9]
        [3 8 11]
        [3 8 10]
        [3 8 9]
        [4 6 11]
        [4 7 11]
        [4 7 10]
        [4 8 11]
        [4 8 10]
        [4 8 9]
        ]);
    % SOSA N term's rules 1
	SOSA_N_rules = single([
        [4 7 9]
        ]);
    % SOSA S term's rules 0
    SOSA_S_rules = single([]);
    % SOSA W term's rules 1
    SOSA_W_rules = single([
        [4 6 10]
        ]);
    % SOSA NS term's rules 10
    SOSA_NS_rules = single([
        [4 6 9]
        [5 6 11]
        [5 6 10]
        [5 6 9]
        [5 7 11]
        [5 7 10]
        [5 7 9]
        [5 8 11]
        [5 8 10]
        [5 8 9]
        ]);
    % Return rules' matrix
    SOSA_rules = [
        SOSA_Z_rules
        SOSA_N_rules
        SOSA_S_rules
        SOSA_W_rules
        SOSA_NS_rules
        ];
end