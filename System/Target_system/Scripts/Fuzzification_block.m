function in_terms_ling_val_belong = Block_processor(PW, TEM, DPE)
     % PW terms
     PW_BN_term = project_pimf(PW,[-4 0 8 20]);
     PW_N_term = project_pimf(PW,[8 20 35 41]);
     PW_S_term = project_pimf(PW,[35 41 67 72]);
     PW_W_term = project_pimf(PW,[67 72 87 91]);
     PW_ALM_term = project_pimf(PW,[87 91 100 101]);
     % TEM terms
     TEM_N_term = project_pimf(TEM,[-15 0 23 31]);
     TEM_S_term = project_pimf(TEM,[23 31 57 65]);
     TEM_W_term = project_pimf(TEM,[57 65 100 101]);
 	% DPE terms
 	DPE_N_term = project_pimf(DPE,[-29 0 35 41]);
    DPE_U_term = project_pimf(DPE,[35 41 80 92]);
    DPE_W_term = project_pimf(DPE,[80 92 101 102]);
    % Return linguistics values' belongingness matrix
    in_terms_ling_val_belong = [
        [PW_BN_term PW_N_term PW_S_term PW_W_term PW_ALM_term]
        [TEM_N_term TEM_S_term TEM_W_term 0 0]
        [DPE_N_term DPE_U_term DPE_W_term 0 0]
        ];
end