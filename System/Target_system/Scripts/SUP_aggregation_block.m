function SUP_aggr_out_terms = Block_processor(SUP_infer_out_terms)
    SUP_aggr_out_terms_temp = single(zeros(1,size(SUP_infer_out_terms,2)));
    
    
    % Aggregation SUP's terms
    for i=1:1:size(SUP_infer_out_terms,2)
        SUP_aggr_out_terms_temp(i) = max(SUP_infer_out_terms(:,i));
    end
    % Return aggregated SUP's terms
    SUP_aggr_out_terms = SUP_aggr_out_terms_temp;
end