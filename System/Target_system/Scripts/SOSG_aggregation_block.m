function SOSG_aggr_out_terms = Block_processor(SOSG_infer_out_terms)
    SOSG_aggr_out_terms_temp = single(zeros(1,size(SOSG_infer_out_terms,2)));
    
    
    % Aggregation SOSG's terms
    for i=1:1:size(SOSG_infer_out_terms,2)
        SOSG_aggr_out_terms_temp(i) = max(SOSG_infer_out_terms(:,i));
    end
    % Return aggregated SOSG's terms
    SOSG_aggr_out_terms = SOSG_aggr_out_terms_temp;
end