function SUP_rules = Block_processor()
    % SUP Z term's rules
    SUP_Z_rules = single([        
        [1 6 11]
        [1 6 10]
        [1 6 9]
        [1 7 11]
        [1 7 10]                       
        [1 7 9]
        [1 8 11]
        [1 8 10]
        [1 8 9]
        [2 6 11]               
        [2 6 10]        
        [2 6 9]
        [2 7 11]
        [2 7 10]
        [2 7 9]
        [2 8 11]
        [2 8 10]
        [2 8 9]
        [3 8 11]
        [3 8 10]
        [3 8 9]
        ]);
    % SUP N term's rules        
	SUP_N_rules = single([
        [3 6 9]
        [3 7 11]
        [3 7 10]
        [3 7 9]
        [4 6 9]
        [4 7 9]
        [4 8 9]
        ]);
    % SUP S term's rules        
    SUP_S_rules = single([
        [3 6 11]
        [3 6 10]
        [4 6 10]
        [4 7 10]
        [4 8 11]
        [4 8 10]        
        ]);
    % SUP W term's rules    
    SUP_W_rules = single([
        [4 7 11]
        [5 6 10]
        [5 6 9]
        [5 7 10]
        [5 7 9]
        [5 8 10]
        [5 8 9]
        ]);
    % SUP NS term's rules        
    SUP_NS_rules = single([
        [4 6 11]        
        [5 6 11]
        [5 7 11]
        [5 8 11]
        ]);
    % Return rules' matrix
    SUP_rules = [
        SUP_Z_rules
        SUP_N_rules
        SUP_S_rules
        SUP_W_rules
        SUP_NS_rules
        ];
end