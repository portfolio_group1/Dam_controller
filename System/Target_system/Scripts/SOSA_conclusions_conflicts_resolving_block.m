function SOSA_resolv_concl_vals = Block_processor(SOSA_concl_vals)
  % SOSA conclusions values sizes
    SOSA_Z_concl_size = 33;
    SOSA_N_concl_size = 1;
    SOSA_S_concl_size = 0;
    SOSA_W_concl_size = 1;
    SOSA_NS_concl_size = 10;
    % SOSA terms' indexes
    SOSA_Z_concl_idx = 1;
    SOSA_N_concl_idx = 2;
    SOSA_S_concl_idx = 3;
    SOSA_W_concl_idx = 4;
    SOSA_NS_concl_idx = 5;
    % SOSA terms resolved conclusions values
    SOSA_Z_resolv_val = single(0);
    SOSA_N_resolv_val = single(0);
    SOSA_S_resolv_val = single(0);
    SOSA_W_resolv_val = single(0);
    SOSA_NS_resolv_val = single(0);
    
    % Resolving SOSA Z term conclusions' conflict
    if (SOSA_Z_concl_size > 1)        
        SOSA_Z_resolv_val = max(SOSA_concl_vals(SOSA_Z_concl_idx,:));
    elseif (SOSA_Z_concl_size == 1)
        SOSA_Z_resolv_val = SOSA_concl_vals(SOSA_Z_concl_idx,1);
    else
        SOSA_Z_resolv_val = 0;
    end
    % Resolving SOSA N term conclusions' conflict
    if (SOSA_N_concl_size > 1)       
        SOSA_N_resolv_val = max(SOSA_concl_vals(SOSA_N_concl_idx,:));
    elseif (SOSA_N_concl_size == 1)
        SOSA_N_resolv_val = SOSA_concl_vals(SOSA_N_concl_idx,1);
    else
        SOSA_N_resolv_val = 0;
    end
    % Resolving SOSA S term conclusions' conflict
    if (SOSA_S_concl_size > 1)        
         SOSA_S_resolv_val = max(SOSA_concl_vals(SOSA_S_concl_idx,:));
    elseif (SOSA_S_concl_size == 1)
        SOSA_S_resolv_val = SOSA_concl_vals(SOSA_S_concl_idx,1);
    else
        SOSA_S_resolv_val = 0;
    end
    % Resolving SOSA W term conclusions' conflict
    if (SOSA_W_concl_size > 1)
        SOSA_W_resolv_val = max(SOSA_concl_vals(SOSA_W_concl_idx,:));
    elseif (SOSA_W_concl_size == 1)
        SOSA_W_resolv_val = SOSA_concl_vals(SOSA_W_concl_idx,1);
    else
        SOSA_W_resolv_val = 0;
    end
    % Resolving SOSA NS term conclusions' conflict
    if (SOSA_NS_concl_size > 1)        
        SOSA_NS_resolv_val = max(SOSA_concl_vals(SOSA_NS_concl_idx,:));
    elseif (SOSA_NS_concl_size == 1)
        SOSA_NS_resolv_val = SOSA_concl_vals(SOSA_NS_concl_idx,1);
    else
        SOSA_NS_resolv_val = 0;
    end
    % Return SUP resolved conclusions values
    SOSA_resolv_concl_vals = [
        SOSA_Z_resolv_val
        SOSA_N_resolv_val
        SOSA_S_resolv_val
        SOSA_W_resolv_val
        SOSA_NS_resolv_val
        ];
end