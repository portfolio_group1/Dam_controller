function SOSG_concl_vals = Block_processor(SOSG_rules,in_terms_ling_val_belong)
    % SOSG conclusions values sizes
    SOSG_Z_concl_size = 18;
    SOSG_N_concl_size = 4;
    SOSG_S_concl_size = 8;
    SOSG_W_concl_size = 0;
    SOSG_NS_concl_size = 15;
    % SOSG conclusions values
    SOSG_Z_concl_vals = single(zeros(1,18));
    SOSG_N_concl_vals = single(zeros(1,18));
    SOSG_S_concl_vals = single(zeros(1,18));
    SOSG_W_concl_vals = single(zeros(1,18));
    SOSG_NS_concl_vals = single(zeros(1,18));
    
    % Calculate Z term's conclusions values
    SOSG_rule_vector_size = 3;
    SOSG_Z_rule_vector = zeros(1,SOSG_rule_vector_size);
    for i=1:1:SOSG_Z_concl_size
        SOSG_Z_rule_vector = SOSG_rules(i,:);
        % Decode SOSG's rule vector
        SOSG_Z_rule_vector = SOSG_rules_decoder(SOSG_rule_vector_size,SOSG_Z_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSG Z term's conclusion value
        SOSG_Z_concl_vals(i) = prod(SOSG_Z_rule_vector);
    end
    % Calculate N term's conclusions values
    SOSG_rule_vector_size = 3;
    SOSG_N_rule_vector = zeros(1,SOSG_rule_vector_size);
    for i=1:1:SOSG_N_concl_size
        SOSG_N_rule_vector = SOSG_rules((i+SOSG_Z_concl_size),:);
        % Decode SOSG's rule vector
        SOSG_N_rule_vector = SOSG_rules_decoder(SOSG_rule_vector_size,SOSG_N_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSG N term's conclusion value
        SOSG_N_concl_vals(i) = prod(SOSG_N_rule_vector);
    end
    % Calculate S term's conclusions values
    SOSG_rule_vector_size = 3;
    SOSG_S_rule_vector = zeros(1,SOSG_rule_vector_size);
    for i=1:1:SOSG_S_concl_size
        SOSG_S_rule_vector = SOSG_rules((i+SOSG_Z_concl_size+SOSG_N_concl_size),:);
        % Decode SOSG's rule vector
        SOSG_S_rule_vector = SOSG_rules_decoder(SOSG_rule_vector_size,SOSG_S_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSG S term's conclusion value
        SOSG_S_concl_vals(i) = prod(SOSG_S_rule_vector);
    end
     % Calculate W term's conclusions values
    SOSG_rule_vector_size = 3;
    SOSG_W_rule_vector = zeros(1,SOSG_rule_vector_size);
    for i=1:1:SOSG_W_concl_size
        SOSG_W_rule_vector = SOSG_rules((i+SOSG_Z_concl_size+SOSG_N_concl_size+SOSG_S_concl_size),:);
        % Decode SOSG's rule vector
        SOSG_W_rule_vector = SOSG_rules_decoder(SOSG_rule_vector_size,SOSG_W_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSG S term's conclusion value
        SOSG_W_concl_vals(i) = prod(SOSG_W_rule_vector);
    end
    % Calculate NS term's conclusions values
    SOSG_rule_vector_size = 3;
    SOSG_NS_rule_vector = zeros(1,SOSG_rule_vector_size);
    for i=1:1:SOSG_NS_concl_size
        SOSG_NS_rule_vector = SOSG_rules((i+SOSG_Z_concl_size+SOSG_N_concl_size+SOSG_S_concl_size+SOSG_W_concl_size),:);
        % Decode SOSG's rule vector
        SOSG_NS_rule_vector = SOSG_rules_decoder(SOSG_rule_vector_size,SOSG_NS_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSG S term's conclusion value
        SOSG_NS_concl_vals(i) = prod(SOSG_NS_rule_vector);
    end
     % Return SOSG conclusions values
    SOSG_concl_vals = [
        SOSG_Z_concl_vals
        SOSG_N_concl_vals
        SOSG_S_concl_vals
        SOSG_W_concl_vals
        SOSG_NS_concl_vals
        ];
end