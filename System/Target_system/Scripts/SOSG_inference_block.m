function SOSG_resolv_out_terms = Block_processor(SOSG_resolv_concl_vals)
    % SOSG terms' indexes
    SOSG_Z_term_idx = 1;
    SOSG_N_term_idx = 2;
    SOSG_S_term_idx = 3;
    SOSG_W_term_idx = 4;
    SOSG_NS_term_idx = 5;
    % SOSG terms
    SOSG_Z_term = project_pimf((0:1E-2:100),[-400 0 4 18]);
    SOSG_N_term = project_pimf((0:1E-2:100),[4 18 19 34]);
    SOSG_S_term = project_pimf((0:1E-2:100),[19 34 49 73]);
    SOSG_W_term = project_pimf((0:1E-2:100),[49 73 75 99]);
    SOSG_NS_term = project_pimf((0:1E-2:100),[75 99 102.5 122.5]);
    
    
    % Inference SOSG Z term
    for i=1:1:size(SOSG_Z_term,2)
        SOSG_Z_term(i) = min(SOSG_Z_term(i),SOSG_resolv_concl_vals(SOSG_Z_term_idx));
    end
    % Inference SOSG N term
    for i=1:1:size(SOSG_N_term,2)
        SOSG_N_term(i) = min(SOSG_N_term(i),SOSG_resolv_concl_vals(SOSG_N_term_idx));
    end
    % Inference SOSG S term
    for i=1:1:size(SOSG_S_term,2)
        SOSG_S_term(i) = min(SOSG_S_term(i),SOSG_resolv_concl_vals(SOSG_S_term_idx));
    end
    % Inference SOSG W term
    for i=1:1:size(SOSG_W_term,2)
        SOSG_W_term(i) = min(SOSG_W_term(i),SOSG_resolv_concl_vals(SOSG_W_term_idx));
    end
    % Inference SOSG NS term
    for i=1:1:size(SOSG_NS_term,2)
        SOSG_NS_term(i) = min(SOSG_NS_term(i),SOSG_resolv_concl_vals(SOSG_NS_term_idx));
    end
    % Return SOSG resolved out terms
    SOSG_resolv_out_terms = [
        SOSG_Z_term
        SOSG_N_term
        SOSG_S_term
        SOSG_W_term
        SOSG_NS_term
        ];
end