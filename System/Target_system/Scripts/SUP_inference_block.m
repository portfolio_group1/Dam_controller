function SUP_infer_out_terms = Block_processor(SUP_resolv_concl_vals)
    % SUP terms' indexes
    SUP_Z_term_idx = 1;
    SUP_N_term_idx = 2;
    SUP_S_term_idx = 3;
    SUP_W_term_idx = 4;
    SUP_NS_term_idx = 5;
    % SUP terms
    SUP_Z_term = project_pimf((0:1E-2:100),[-2 0 4 7]);
    SUP_N_term = project_pimf((0:1E-2:100),[4 7 28 37]);
    SUP_S_term = project_pimf((0:1E-2:100),[28 37 64 68]);
    SUP_W_term = project_pimf((0:1E-2:100),[64 68 87 94]);
    SUP_NS_term = project_pimf((0:1E-2:100),[87 94 100 101]);
    
    
    % Inference SUP Z term
    for i=1:1:size(SUP_Z_term,2)
        SUP_Z_term(i) = min(SUP_Z_term(i),SUP_resolv_concl_vals(SUP_Z_term_idx));
    end
    % Inference SUP N term
    for i=1:1:size(SUP_N_term,2)
        SUP_N_term(i) = min(SUP_N_term(i),SUP_resolv_concl_vals(SUP_N_term_idx));
    end
    % Inference SUP S term
    for i=1:1:size(SUP_S_term,2)
        SUP_S_term(i) = min(SUP_S_term(i),SUP_resolv_concl_vals(SUP_S_term_idx));
    end
    % Inference SUP W term
    for i=1:1:size(SUP_W_term,2)
        SUP_W_term(i) = min(SUP_W_term(i),SUP_resolv_concl_vals(SUP_W_term_idx));
    end
    % Inference SUP NS term
    for i=1:1:size(SUP_NS_term,2)
        SUP_NS_term(i) = min(SUP_NS_term(i),SUP_resolv_concl_vals(SUP_NS_term_idx));
    end
    % Return SUP inferenced out terms
    SUP_infer_out_terms = [
        SUP_Z_term
        SUP_N_term
        SUP_S_term
        SUP_W_term
        SUP_NS_term
        ];
end