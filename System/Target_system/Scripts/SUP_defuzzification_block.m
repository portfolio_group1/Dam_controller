function SUP = Block_processor(SUP_aggr_out_terms)
    defuzz_numerator = single(0);
    defuzz_denominator = single(0);
        
    % Calculate defuzzification's numerator & denominator
    for i=1:1:size(SUP_aggr_out_terms,2)
        defuzz_numerator = (defuzz_numerator+((i-1)*SUP_aggr_out_terms(i)));
        defuzz_denominator = (defuzz_denominator+SUP_aggr_out_terms(i));
    end    
    % Return SUP's sharp value
    SUP = single((defuzz_numerator/defuzz_denominator)/100);
end