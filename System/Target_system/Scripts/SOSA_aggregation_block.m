function SOSA_aggr_out_terms = Block_processor(SOSA_infer_out_terms)
    SOSA_aggr_out_terms_temp = single(zeros(1,size(SOSA_infer_out_terms,2)));
    
    
    % Aggregation SOSA's terms
    for i=1:1:size(SOSA_infer_out_terms,2)
        SOSA_aggr_out_terms_temp(i) = max(SOSA_infer_out_terms(:,i));
    end
    % Return aggregated SOSA's terms
    SOSA_aggr_out_terms = SOSA_aggr_out_terms_temp;
end