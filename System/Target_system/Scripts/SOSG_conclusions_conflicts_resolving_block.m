function SOSG_resolv_concl_vals = Block_processor(SOSG_concl_vals)
    % SOSG conclusions values sizes
    SOSG_Z_concl_size = 18;
    SOSG_N_concl_size = 4;
    SOSG_S_concl_size = 8;
    SOSG_W_concl_size = 0;
    SOSG_NS_concl_size = 15;
    % SOSG terms' indexes
    SOSG_Z_concl_idx = 1;
    SOSG_N_concl_idx = 2;
    SOSG_S_concl_idx = 3;
    SOSG_W_concl_idx = 4;
    SOSG_NS_concl_idx = 5;
    % SOSG terms resolved conclusions values
    SOSG_Z_resolv_val = single(0);
    SOSG_N_resolv_val = single(0);
    SOSG_S_resolv_val = single(0);
    SOSG_W_resolv_val = single(0);
    SOSG_NS_resolv_val = single(0);
    
    % Resolving SOSG Z term conclusions' conflict
    if (SOSG_Z_concl_size > 1)        
        SOSG_Z_resolv_val = max(SOSG_concl_vals(SOSG_Z_concl_idx,:));
    elseif (SOSG_Z_concl_size == 1)
        SOSG_Z_resolv_val = SOSG_concl_vals(SOSG_Z_concl_idx,1);
    else
        SOSG_Z_resolv_val = 0;
    end
    % Resolving SOSG N term conclusions' conflict
    if (SOSG_N_concl_size > 1)        
        SOSG_N_resolv_val = max(SOSG_concl_vals(SOSG_N_concl_idx,:));        
    elseif (SOSG_N_concl_size == 1)
        SOSG_N_resolv_val = SOSG_concl_vals(SOSG_N_concl_idx,1);
    else
        SOSG_N_resolv_val = 0;
    end
    % Resolving SOSG S term conclusions' conflict
    if (SOSG_S_concl_size > 1)
         SOSG_S_resolv_val = max(SOSG_concl_vals(SOSG_S_concl_idx,:));
    elseif (SOSG_S_concl_size == 1)
        SOSG_S_resolv_val = SOSG_concl_vals(SOSG_S_concl_idx,1);
    else
        SOSG_S_resolv_val = 0;
    end
    % Resolving SOSG W term conclusions' conflict
    if (SOSG_W_concl_size > 1)
        SOSG_W_resolv_val = max(SOSG_concl_vals(SOSG_W_concl_idx,:));
    elseif (SOSG_W_concl_size == 1)
        SOSG_W_resolv_val = SOSG_concl_vals(SOSG_W_concl_idx,1);
    else
        SOSG_W_resolv_val = 0;
    end
    % Resolving SOSG NS term conclusions' conflict
    if (SOSG_NS_concl_size > 1)        
        SOSG_NS_resolv_val = max(SOSG_concl_vals(SOSG_NS_concl_idx,:));
    elseif (SOSG_NS_concl_size == 1)
        SOSG_NS_resolv_val = SOSG_concl_vals(SOSG_NS_concl_idx,1);
    else
        SOSG_NS_resolv_val = 0;
    end
    % Return SUP resolved conclusions values
    SOSG_resolv_concl_vals = [
        SOSG_Z_resolv_val
        SOSG_N_resolv_val
        SOSG_S_resolv_val
        SOSG_W_resolv_val
        SOSG_NS_resolv_val
        ];
end