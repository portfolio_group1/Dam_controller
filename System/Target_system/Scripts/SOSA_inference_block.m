function SOSA_resolv_out_terms = Block_processor(SOSA_resolv_concl_vals)
    % SOSA terms' indexes
    SOSA_Z_term_idx = 1;
    SOSA_N_term_idx = 2;
    SOSA_S_term_idx = 3;
    SOSA_W_term_idx = 4;
    SOSA_NS_term_idx = 5;
    % SOSA terms
    SOSA_Z_term = project_pimf((0:1E-2:100),[-400 0 4 18]);
    SOSA_N_term = project_pimf((0:1E-2:100),[4 18 19 34]);
    SOSA_S_term = project_pimf((0:1E-2:100),[19 34 49 73]);
    SOSA_W_term = project_pimf((0:1E-2:100),[49 73 75 99]);
    SOSA_NS_term = project_pimf((0:1E-2:100),[75 99 102.5 122.5]);
    
    
    % Inference SOSA Z term
    for i=1:1:size(SOSA_Z_term,2)
        SOSA_Z_term(i) = min(SOSA_Z_term(i),SOSA_resolv_concl_vals(SOSA_Z_term_idx));
    end
    % Inference SOSA N term
    for i=1:1:size(SOSA_N_term,2)
        SOSA_N_term(i) = min(SOSA_N_term(i),SOSA_resolv_concl_vals(SOSA_N_term_idx));
    end
    % Inference SOSA S term
    for i=1:1:size(SOSA_S_term,2)
        SOSA_S_term(i) = min(SOSA_S_term(i),SOSA_resolv_concl_vals(SOSA_S_term_idx));
    end
    % Inference SOSA W term
    for i=1:1:size(SOSA_W_term,2)
        SOSA_W_term(i) = min(SOSA_W_term(i),SOSA_resolv_concl_vals(SOSA_W_term_idx));
    end
    % Inference SOSA NS term
    for i=1:1:size(SOSA_NS_term,2)
        SOSA_NS_term(i) = min(SOSA_NS_term(i),SOSA_resolv_concl_vals(SOSA_NS_term_idx));
    end
    % Return SOSA resolved out terms
    SOSA_resolv_out_terms = [
        SOSA_Z_term
        SOSA_N_term
        SOSA_S_term
        SOSA_W_term
        SOSA_NS_term
        ];
end