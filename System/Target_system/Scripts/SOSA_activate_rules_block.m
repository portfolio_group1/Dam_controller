function SOSA_concl_vals = Block_processor(SOSA_rules,in_terms_ling_val_belong)
    % SOSA conclusions values sizes
    SOSA_Z_concl_size = 33;
    SOSA_N_concl_size = 1;
    SOSA_S_concl_size = 0;
    SOSA_W_concl_size = 1;
    SOSA_NS_concl_size = 10;
    % SOSA conclusions values
    SOSA_Z_concl_vals = single(zeros(1,33));
    SOSA_N_concl_vals = single(zeros(1,33));
    SOSA_S_concl_vals = single(zeros(1,33));
    SOSA_W_concl_vals = single(zeros(1,33));
    SOSA_NS_concl_vals = single(zeros(1,33));
          
    % Calculate Z term's conclusions values
    SOSA_rule_vector_size = 3;
    SOSA_Z_rule_vector = zeros(1,SOSA_rule_vector_size);
    for i=1:1:SOSA_Z_concl_size
        SOSA_Z_rule_vector = SOSA_rules(i,:);
        % Decode SOSA's rule vector
        SOSA_Z_rule_vector = SOSA_rules_decoder(SOSA_rule_vector_size,SOSA_Z_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSA Z term's conclusion value
        SOSA_Z_concl_vals(i) = prod(SOSA_Z_rule_vector);
    end    
    % Calculate N term's conclusions values
    SOSA_rule_vector_size = 3;
    SOSA_N_rule_vector = zeros(1,SOSA_rule_vector_size);
    for i=1:1:SOSA_N_concl_size
        SOSA_N_rule_vector = SOSA_rules((i+SOSA_Z_concl_size),:);
        % Decode SOSA's rule vector
        SOSA_N_rule_vector = SOSA_rules_decoder(SOSA_rule_vector_size,SOSA_N_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSA N term's conclusion value
        SOSA_N_concl_vals(i) = prod(SOSA_N_rule_vector);
    end
    % Calculate S term's conclusions values
    SOSA_rule_vector_size = 3;
    SOSA_S_rule_vector = zeros(1,SOSA_rule_vector_size);
    for i=1:1:SOSA_S_concl_size
        SOSA_S_rule_vector = SOSA_rules((i+SOSA_Z_concl_size+SOSA_N_concl_size),:);
        % Decode SOSA's rule vector
        SOSA_S_rule_vector = SOSA_rules_decoder(SOSA_rule_vector_size,SOSA_S_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSA S term's conclusion value
        SOSA_S_concl_vals(i) = prod(SOSA_S_rule_vector);
    end
    % Calculate W term's conclusions values
    SOSA_rule_vector_size = 3;
    SOSA_W_rule_vector = zeros(1,SOSA_rule_vector_size);
    for i=1:1:SOSA_W_concl_size
        SOSA_W_rule_vector = SOSA_rules((i+SOSA_Z_concl_size+SOSA_N_concl_size+SOSA_S_concl_size),:);
        % Decode SOSA's rule vector
        SOSA_W_rule_vector = SOSA_rules_decoder(SOSA_rule_vector_size,SOSA_W_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSA S term's conclusion value
        SOSA_W_concl_vals(i) = prod(SOSA_W_rule_vector);
    end
    % Calculate NS term's conclusions values
    SOSA_rule_vector_size = 3;
    SOSA_NS_rule_vector = zeros(1,SOSA_rule_vector_size);
    for i=1:1:SOSA_NS_concl_size
        SOSA_NS_rule_vector = SOSA_rules((i+SOSA_Z_concl_size+SOSA_N_concl_size+SOSA_S_concl_size+SOSA_W_concl_size),:);
        % Decode SOSA's rule vector
        SOSA_NS_rule_vector = SOSA_rules_decoder(SOSA_rule_vector_size,SOSA_NS_rule_vector,in_terms_ling_val_belong);
        % Calculate SOSA S term's conclusion value
        SOSA_NS_concl_vals(i) = prod(SOSA_NS_rule_vector);
    end
    % Return SOSA conclusions values
    SOSA_concl_vals = [
        SOSA_Z_concl_vals
        SOSA_N_concl_vals
        SOSA_S_concl_vals
        SOSA_W_concl_vals
        SOSA_NS_concl_vals
        ];
end