function decoded_SOSA_rule_vector_out = SOSA_rules_decoder(SOSA_rule_vector_size,SOSA_rule_vector_in,in_terms_ling_val_belong)
    SOSA_rule_vector_out_temp = single(zeros(1,SOSA_rule_vector_size));
    % PW terms' indexes
    PW_idx = 1;
    PW_BN_idx = 1;
    PW_N_idx = 2;
    PW_S_idx = 3;
    PW_W_idx = 4;
    PW_ALM_idx = 5;    
    % TEM terms' indexes
    TEM_idx = 2;
    TEM_N_idx = 1;
    TEM_S_idx = 2;
    TEM_W_idx = 3;
    % DPE terms' indexes
    DPE_idx = 3;
    DPE_N_idx = 1;
    DPE_U_idx = 2;
    DPE_W_idx = 3;
    
    
    % Decode SOSA's rule vector
    for i=1:1:SOSA_rule_vector_size
        if (SOSA_rule_vector_in(i) == 1)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_BN_idx);
        elseif (SOSA_rule_vector_in(i) == 2)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_N_idx);
        elseif (SOSA_rule_vector_in(i) == 3)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_S_idx);
        elseif (SOSA_rule_vector_in(i) == 4)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_W_idx);
        elseif (SOSA_rule_vector_in(i) == 5)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(PW_idx,PW_ALM_idx);
        elseif (SOSA_rule_vector_in(i) == 6)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_N_idx);
        elseif (SOSA_rule_vector_in(i) == 7)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_S_idx);
        elseif (SOSA_rule_vector_in(i) == 8)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_W_idx);
        elseif (SOSA_rule_vector_in(i) == 9)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(DPE_idx,DPE_N_idx);
        elseif (SOSA_rule_vector_in(i) == 10)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(DPE_idx,DPE_U_idx);
        elseif (SOSA_rule_vector_in(i) == 11)
            SOSA_rule_vector_out_temp(i) = in_terms_ling_val_belong(DPE_idx,DPE_W_idx);
        end
    end
    % Return decoded SOSA's rule vector
    decoded_SOSA_rule_vector_out = SOSA_rule_vector_out_temp;
end