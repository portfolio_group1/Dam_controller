classdef System_core_Test < matlab.unittest.TestCase
    properties        
    end
    
    methods(Test)
        function System_core_When_inference_process(test)            
        end
        function System_core_When_aggregation_process(test)
             % ### arrange ###
             % Test's vectors
             PW = [71.186 0];
             TEM = [27.31 0];
             DPE = [85.11 0];
             tests_vec_size = 1;
             % Generate rules
             SUP_rules = SUP_rules_block();
             SOSG_rules = SOSG_rules_block();
             SOSA_rules = SOSA_rules_block();
             % Calculate input terms linguistics values's belongingness            
             for i=1:1:tests_vec_size
                 in_terms_ling_val_belong = Fuzzification_block(PW(i),TEM(i),DPE(i));
             end
             % Calculate conclusions values
             SUP_concl_vals = SUP_activate_rules_block(SUP_rules,in_terms_ling_val_belong);
             SOSG_concl_vals = SOSG_activate_rules_block(SOSG_rules,in_terms_ling_val_belong);
             SOSA_concl_vals = SOSA_activate_rules_block(SOSA_rules,in_terms_ling_val_belong);
             % Resolve conclusions values' conflicts
             SUP_resolv_concl_vals = SUP_conclusions_conflicts_resolving_block(SUP_concl_vals);
             SOSG_resolv_concl_vals = SOSG_conclusions_conflicts_resolving_block(SOSG_concl_vals);
             SOSA_resolv_concl_vals = SOSA_conclusions_conflicts_resolving_block(SOSA_concl_vals);    
             % Inference resolved conclusions' values
             SUP_infer_out_terms = SUP_inference_block(SUP_resolv_concl_vals);
             SOSG_infer_out_terms = SOSG_inference_block(SOSG_resolv_concl_vals);
             SOSA_infer_out_terms = SOSA_inference_block(SOSA_resolv_concl_vals);            
             % Aggregated out terms samples            
             % ### act ###            
             % Aggregation SUP's terms
             SUP_aggr_out_terms = SUP_aggregation_block(SUP_infer_out_terms);        
             for i=1:1:size(SUP_infer_out_terms,2)
                 SUP_aggr_out_terms_sampls(i) = max(SUP_infer_out_terms(:,i));
             end
             % Aggregation SOSG's terms
             SOSG_aggr_out_terms = SOSG_aggregation_block(SOSG_infer_out_terms);
             for i=1:1:size(SOSG_infer_out_terms,2)
                 SOSG_aggr_out_terms_sampls(i) = max(SOSG_infer_out_terms(:,i));
             end
             % Aggregation SOSA's terms
             SOSA_aggr_out_terms = SOSA_aggregation_block(SOSA_infer_out_terms);
             for i=1:1:size(SOSA_infer_out_terms,2)
                 SOSA_aggr_out_terms_sampls(i) = max(SOSA_infer_out_terms(:,i));
             end
             % ### assert ###
             % Test entire aggregated SUP's out terms
              for i=1:1:size(SUP_aggr_out_terms,2)
                  test.verifyEqual(SUP_aggr_out_terms(i),SUP_aggr_out_terms_sampls(i),"In entire SUP's aggregation sample: "+int2str(i));
              end
              % Test entire aggregated SOSG's out terms
              for i=1:1:size(SOSG_aggr_out_terms,2)
                  test.verifyEqual(SOSG_aggr_out_terms(i),SOSG_aggr_out_terms_sampls(i),"In entire SOSG's aggregation sample: "+int2str(i));
              end
             % Test entire aggregated SOSA's out terms
             for i=1:1:size(SOSA_aggr_out_terms,2)
                 test.verifyEqual(SOSA_aggr_out_terms(i),SOSA_aggr_out_terms_sampls(i),"In entire SOSA's aggregation sample: "+int2str(i));
             end
        end
        function System_core_When_defuzzification_process(test)
            % ### arrange ###
            % Test's vectors
            PW = [71.186 0];
            TEM = [27.31 0];
            DPE = [85.11 0];
            tests_vec_size = 1;
            max_err = 1E-1;
            % Generate rules
            SUP_rules = SUP_rules_block();
            SOSG_rules = SOSG_rules_block();
            SOSA_rules = SOSA_rules_block();
            % Calculate input terms linguistics values's belongingness
            for i=1:1:tests_vec_size
                in_terms_ling_val_belong = Fuzzification_block(PW(i),TEM(i),DPE(i));
            end
            % Calculate conclusions values
            SUP_concl_vals = SUP_activate_rules_block(SUP_rules,in_terms_ling_val_belong);
            SOSG_concl_vals = SOSG_activate_rules_block(SOSG_rules,in_terms_ling_val_belong);
            SOSA_concl_vals = SOSA_activate_rules_block(SOSA_rules,in_terms_ling_val_belong);
            % Resolve conclusions values' conflicts
            SUP_resolv_concl_vals = SUP_conclusions_conflicts_resolving_block(SUP_concl_vals);
            SOSG_resolv_concl_vals = SOSG_conclusions_conflicts_resolving_block(SOSG_concl_vals);
            SOSA_resolv_concl_vals = SOSA_conclusions_conflicts_resolving_block(SOSA_concl_vals);    
            % Inference resolved conclusions' values
            SUP_infer_out_terms = SUP_inference_block(SUP_resolv_concl_vals);
            SOSG_infer_out_terms = SOSG_inference_block(SOSG_resolv_concl_vals);
            SOSA_infer_out_terms = SOSA_inference_block(SOSA_resolv_concl_vals);
            % Aggregation inferenced out terms
            SUP_aggr_out_terms = SUP_aggregation_block(SUP_infer_out_terms);
            SOSG_aggr_out_terms = SOSG_aggregation_block(SOSG_infer_out_terms);
            SOSA_aggr_out_terms = SOSA_aggregation_block(SOSA_infer_out_terms);
            % ### act ###
            % Defuzzification SUP's value
            SUP = SUP_defuzzification_block(SUP_aggr_out_terms);
            SUP_sample_defuzz_numerator = single(0);
            SUP_sample_defuzz_denominator = single(0);
            % Calculate SUP defuzzification's numerator
            for i=1:1:size(SUP_aggr_out_terms,2)
                SUP_sample_defuzz_numerator = (SUP_sample_defuzz_numerator+((i-1)*SUP_aggr_out_terms(i)));
            end
            % Calculate SUP defuzzification's denominator
            SUP_sample_defuzz_denominator = sum(SUP_aggr_out_terms);
            % Calculate SUP sample's sharp value
            SUP_sample = ((SUP_sample_defuzz_numerator/SUP_sample_defuzz_denominator)/100);
            % Defuzzification SOSG's value
            SOSG = SOSG_defuzzification_block(SOSG_aggr_out_terms);
            SOSG_sample_defuzz_numerator = 0;
            SOSG_sample_defuzz_denominator = 0;
            % Defuzzification SOSG's value
            SOSG = SOSG_defuzzification_block(SOSG_aggr_out_terms);
            % Calculate SOSG defuzzification's numerator
            for i=1:1:size(SOSG_aggr_out_terms,2)
                SOSG_sample_defuzz_numerator = (SOSG_sample_defuzz_numerator+(i*SOSG_aggr_out_terms(i)));
            end
            % Calculate SOSG defuzzification's denominator
            SOSG_sample_defuzz_denominator = sum(SOSG_aggr_out_terms);
            % Calculate SOSG sample's sharp value
            SOSG_sample = ((SOSG_sample_defuzz_numerator/SOSG_sample_defuzz_denominator)/100);
            % Defuzzification SOSA's value
            SOSA = (SOSA_defuzzification_block(SOSA_aggr_out_terms)/100);
            SOSA_sample_defuzz_numerator = 0;
            SOSA_sample_defuzz_denominator = 0;    
            % Defuzzification SOSA's value
            SOSA = SOSA_defuzzification_block(SOSA_aggr_out_terms);
            % Calculate SOSA defuzzification's numerator
            for i=1:1:size(SOSA_aggr_out_terms,2)
                SOSA_sample_defuzz_numerator = (SOSA_sample_defuzz_numerator+(i*SOSA_aggr_out_terms(i)));
            end
            % Calculate SOSA defuzzification's denominator
            SOSA_sample_defuzz_denominator = sum(SOSA_aggr_out_terms);
            % Calculate SOSA sample's sharp value
            SOSA_sample = ((SOSA_sample_defuzz_numerator/SOSA_sample_defuzz_denominator)/100);
            % ### assert ###
            % SUP's sharp value            
            SUP_comp = ((abs(SUP-SUP_sample))<=max_err);
            test.verifyEqual(SUP_comp,true,"SUP's sharp value");
            % SOSG's sharp value
            SOSG_comp = ((abs(SOSG-SOSG_sample))<=max_err);
            test.verifyEqual(SOSG_comp,true,"SOSG's sharp value");
            % SOSA's sharp value
            SOSA_comp = ((abs(SOSA-SOSA_sample))<=max_err);
            test.verifyEqual(SOSA_comp,true,"SOSA's sharp value");
        end
    end
end