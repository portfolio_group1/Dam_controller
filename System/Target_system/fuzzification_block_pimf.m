function y = fuzzification_block_pimf(x,params)
    temp_y = single(zeros(1,size(x,2)));
    a = params(1);
    b = params(2);
    c = params(3);
    d = params(4);

    for i=1:1:size(x,2)        
        if (x(i) <= a)
            temp_y(i) = 0;
        elseif ((x(i) >= a) && (x(i) <= ((a+b)/2)))
            temp_y(i) = (2*(((x(i)-a)/(b-a))^2));
        elseif ((x(i) >= ((a+b)/2)) && (x(i) <= b))
            temp_y(i) = (1-(2*(((x(i)-b)/(b-a))^2)));
        elseif ((x(i) >= b) && (x(i) <= c))
            temp_y(i) = 1;
        elseif ((x(i) >= c) && (x(i) <= ((c+d)/2)))
            temp_y(i) = (1-(2*(((x(i)-c)/(d-c))^2)));
        elseif ((x(i) >= ((c+d)/2)) && (x(i) <= d))
            temp_y(i) = (2*(((x(i)-d)/(d-c))^2));
        elseif (x(i) >= d)
            temp_y(i) = 0;
        end        
    end
    y = temp_y;
end