function decoded_TEM_DEC_rule_vector_out = TEM_DEC_rules_decoder(TEM_DEC_rule_vector_size,TEM_DEC_rule_vector_in,in_terms_ling_val_belong)
    TEM_DEC_rule_vector_out_temp = uint8(zeros(1,TEM_DEC_rule_vector_size));
    % TEM terms' indexes
    TEM_idx = 1;
    TEM_BZ_idx = 1;
    TEM_Z_idx = 2;
    TEM_ML_idx = 3;
    TEM_L_idx = 4;
    TEM_C_idx = 5;
    TEM_BC_idx = 6;
    TEM_G_idx = 7;


    % Decode TEM_DEC's rule vector
	for i=1:1:TEM_DEC_rule_vector_size
        if (TEM_DEC_rule_vector_in(i) == 1)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_BZ_idx);
        elseif (TEM_DEC_rule_vector_in(i) == 2)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_Z_idx);
        elseif (TEM_DEC_rule_vector_in(i) == 3)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_ML_idx);
        elseif (TEM_DEC_rule_vector_in(i) == 4)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_L_idx);
        elseif (TEM_DEC_rule_vector_in(i) == 5)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_C_idx);            
        elseif (TEM_DEC_rule_vector_in(i) == 6)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_BC_idx);
        elseif (TEM_DEC_rule_vector_in(i) == 7)
            TEM_DEC_rule_vector_out_temp(i) = in_terms_ling_val_belong(TEM_idx,TEM_G_idx);
        end
    end
     % Return decoded TEM_DEC's rule vector
    decoded_TEM_DEC_rule_vector_out = TEM_DEC_rule_vector_out_temp;
end