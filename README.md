### It is the project of the dam controller based on the Type-1 fuzzy logic and expert system (the hybrid system). ###

---

### Brief description of the system

The system receives the signals from the system's sensors.

These are processed by the input decoders (some of them are based on an expert system). Subsequently, the core (fuzzy logic module) interprets the signals and returns values which are intercepted by the output decoders. Then signals are transferred to the interface of the system (the API).

Input decoders are used to convert the sensors' values into percentages (core input values), however the output decoders change the percentages (core output values) into output devices' values.

Each decoder is scalable (both inputs and outputs), it means that it is possible to set the minimum and maximum value to adapt the system to the deployment environment.

---

### Terms abbreviations

PW  : Water level

TEM : Temperature

DPE : Energy consumption limit

SUP  : State of pump use

SOSG : State of opening the main lock

SOSA : State of opening the emergency lock

### The limits currently established are
PW ∈ <0, 500> [m]

TEMP ∈ <-20, 40> [℃]

DPE ∈ <0, 1000> [kWh]

SUP ∈ <0, 100> [unit of the state of using the pump]

SOSG ∈ <0, 1000> [unit of the state of opening the main lock]

SOSA ∈ <0, 1000> [unit of the state of opening the main lock]

All terms based on the Pi-shaped membership function (pimf)

---

### Catalogues
1. Schematics, here are schematics describing:
   - System architecture: Main system preview (from the customer side),
   - Hybrid system concept: It describes the modules included in the fuzzy logic and expert system units,
   - System model: Description of all the system's modules (including sensors, decoders, core and so forth).
2. Documentation : Most of all the one describes the fuzzy logic and expert system terms. In addition, there is everything needed to describe the previous one, tables, scripts and figures.
3. System : It consists of the:
   - Output terms models: Here are conceptual output terms modules created using the "Fuzzy Logic Toolbox". These modules were used to test the implemented system and finally, the one matched the Fuzzy Logic Toolbox's modules,
   - Target system: The Simulink file containing the main system model (System_model.slx) and other necessary files.

### Tools used to implement the project
1. MATLAB R2021b,
2. Simulink.